from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt
# Register your models here.

# Feature 4 - Register the three models with the admin 
# so that you can see them in the Django admin site.

# Feature 4 ExpenseCategory Admin Registry
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "owner",
    ]


# Feature 4 Account Admin Registry
@admin.register(Account)
class Account(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
    ]    

# Feature 4 Receipt Admin Registry
@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
        "date",
    ]    