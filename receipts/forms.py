# Feature 11 create a recipe view form
from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

# Feature 13- ExpenceCategory Form 
        
class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]        

# Feature 14 - AccountForm 

class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number"
        ]                