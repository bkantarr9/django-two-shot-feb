from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
# Feature 5 a list view for Receipt Model
@login_required(login_url="login") # Feature 8 Login required and receipts = Receipt.objects.all() changed, used filter
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/list.html", context)



# Feature 11 create receipt view  

@login_required(login_url="login")   #Feature 11-2. 
def create_receipt(request):         #Feature 11-3.
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)

#Feature 12-1.
#Feature 12- category list view - login required
@login_required(login_url="login")
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    receipts = Receipt.objects.all()
    context = {
        "category_list": categories,
        "receipts": receipts,
    }
    return render(request, "receipts/categories.html", context)


#Feature 12- account list view- login required
@login_required(login_url="login")
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    receipts = Receipt.objects.all()
    context = {
        "account_list": accounts,
        "receipts": receipts
    }
    return render(request, "receipts/accounts.html", context)


# Feature 13 - create category view

@login_required(login_url="login")
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)



# Feature 14 - Create account view 

@login_required(login_url="login")
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
